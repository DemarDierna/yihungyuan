class GameManager extends Phaser.State{

    private normalGame: NormalGame;
    private freeGame: FreeGame;
    private server: Server;
    private gameData: GameData;
    private errorMessage: ErrorMessage;

    constructor(){
        super();
    }

    init(server: Server, gameData: GameData){
        this.gameData = gameData;
        this.server = server;
        this.server.connectionSignal.add(this.connectionHandler, this);
        this.server.receiveSignal.add(this.receiveHandler, this);
        
        this.normalGame = new NormalGame(this.game, this.gameData);
        this.normalGame.sendMessageSignal.add(this.sendHandler, this);
        
        this.freeGame = new FreeGame(this.game, this.gameData);
        this.freeGame.visible = false;

        this.errorMessage = new ErrorMessage(this.game);
        this.add.existing(this.errorMessage);
    }

    /**
     * 伺服器連接處理程序
     * @param connectionStatus 伺服器連接狀態
     */
    private connectionHandler(connectionStatus: boolean){
        if(!connectionStatus){
            this.errorMessage.showError();
        }
    }

    /**
     * 伺服器資料處理程序
     * @param data 伺服器傳來的資料
     */
    private receiveHandler(data: string){
        let json = JSON.parse(data);
        switch(json.Code){
            case PACKET_CODE.GtoCGameError:
                // this.errorMessage.showError(json.packet.errorMessage);
                this.server.close();
                break;
            case PACKET_CODE.GtoCSlotNGPlay:
                console.log(json);
                this.normalGame.changeReelStatus(REEL_STATUS.ROLLING, json);
                break;
        }
    }

    private sendHandler(data: string){
        this.server.send(data);
    }
}