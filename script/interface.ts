interface GameData{
    money?: number;
    moneyFractionMultiple?: number;
    denom?: number;
    line?: number;
    betMultiples?: number[];
    betUnit?: number;
}