class Reel extends Phaser.Group{

    public rollFinishedSignal: Phaser.Signal;
    private intervalSymbolCount: number;
    private intervalSymbolGroup: Phaser.Group[];
    private mainSymbolCount: number;
    private mainSymbolGroup: Phaser.Group[];
    private previousGroupIndex: number;
    private nextGroupIndex: number;
    private settings: any;
    private offsetX: number;
    private offsetY: number;
    private symbolNames: string[];
    private rollingTime: number;
    private rollTimerEvent: Phaser.TimerEvent;

    constructor(game: Phaser.Game){
        super(game);
    }

    public init(stripIndex: number, reelSettings: any){
        this.rollFinishedSignal = new Phaser.Signal();
        this.settings = reelSettings;
        this.intervalSymbolGroup = new Array(2);
        this.intervalSymbolGroup[0] = this.game.add.group(this);
        this.intervalSymbolGroup[1] = this.game.add.group(this);
        this.mainSymbolGroup = new Array(2);
        this.mainSymbolGroup[0] = this.game.add.group(this);
        this.mainSymbolGroup[1] = this.game.add.group(this);
        this.previousGroupIndex = 0;
        this.nextGroupIndex = 1;
        this.offsetX = this.settings.PositionX[stripIndex];
        this.offsetY = this.settings.PositionY[stripIndex];
        this.intervalSymbolCount = this.settings.SymbolIntervalCount[stripIndex];
        this.mainSymbolCount = this.settings.InitialPermutations[stripIndex].length;
        this.symbolNames = ["WD", "SC", "FG", "S1", "S2", "S3", "S4", "A", "K", "Q", "J", "TE", "NI"];
        this.rollingTime = this.settings.RollingTime[stripIndex];

        this.mainSymbolGroup[this.previousGroupIndex].position.set(this.offsetX, this.offsetY);
        for(let i: number = 0; i < this.mainSymbolCount; i++){
            let symbol: Phaser.Sprite = this.game.add.sprite(0, this.settings.SymbolHeight * i, 'Symbol', this.symbolNames[this.settings.InitialPermutations[stripIndex][i]], this.mainSymbolGroup[this.previousGroupIndex]);
            symbol.anchor.set(0.5);
        }

        let randomSymbol: number[] = this.generateRandomSymbol();
        this.intervalSymbolGroup[this.previousGroupIndex].position.set(this.offsetX, this.offsetY + this.settings.SymbolHeight * this.mainSymbolCount);
        for(let i: number = 0; i < this.intervalSymbolCount; i++){
            let symbol: Phaser.Sprite = this.game.add.sprite(0, this.settings.SymbolHeight * i, 'Symbol', this.symbolNames[randomSymbol[i]], this.intervalSymbolGroup[this.previousGroupIndex]);
            symbol.anchor.set(0.5);
        }
    }

    public prepareToRolling(mainSymbol: number[]){
        this.intervalSymbolGroup[this.nextGroupIndex].children.length = 0;
        this.intervalSymbolGroup[this.nextGroupIndex].position.set(this.offsetX, this.offsetY - this.settings.SymbolHeight * this.intervalSymbolCount);
        let randomSymbol: number[] = this.generateRandomSymbol();
        for(let i: number = 0; i < this.intervalSymbolCount; i++){
            let symbol: Phaser.Sprite = this.game.add.sprite(0, this.settings.SymbolHeight * i, 'Symbol', this.symbolNames[randomSymbol[i]], this.intervalSymbolGroup[this.nextGroupIndex]);
            symbol.anchor.set(0.5);
        }

        this.mainSymbolGroup[this.nextGroupIndex].children.length = 0;
        this.mainSymbolGroup[this.nextGroupIndex].position.set(this.offsetX, this.offsetY - this.settings.SymbolHeight * (this.intervalSymbolCount + this.mainSymbolCount));
        for(let i: number = 0; i < this.mainSymbolCount; i++){
            let symbol: Phaser.Sprite = this.game.add.sprite(0, this.settings.SymbolHeight * i, 'Symbol', this.symbolNames[mainSymbol[i]], this.mainSymbolGroup[this.nextGroupIndex]);
            symbol.anchor.set(0.5);
        }
        
        if(this.settings.RollBeginningOffset > 0){
            this.rollBeginningOffset();
        }else{
            this.startRolling();
        }
    }

    private rollBeginningOffset(){
        let perFrameOffset: number = this.settings.RollBeginningOffset / (this.settings.RollBeginningTotalTime / 20);
        let intervalNextPosY: number = this.intervalSymbolGroup[this.previousGroupIndex].position.y;
        let intervalTargetPosY: number = intervalNextPosY - this.settings.RollBeginningOffset;
        let mainNextPosY: number = this.mainSymbolGroup[this.previousGroupIndex].position.y;
        let mainTargetPosY: number = mainNextPosY - this.settings.RollBeginningOffset;

        this.rollTimerEvent = this.game.time.events.loop(20, ()=>{
            intervalNextPosY -= perFrameOffset;
            mainNextPosY -= perFrameOffset;
            if(mainNextPosY <= mainTargetPosY){
                this.game.time.events.remove(this.rollTimerEvent);
                this.intervalSymbolGroup[this.previousGroupIndex].position.set(this.offsetX, intervalTargetPosY);
                this.mainSymbolGroup[this.previousGroupIndex].position.set(this.offsetX, mainTargetPosY);
                this.startRolling();
                return;
            }
            this.intervalSymbolGroup[this.previousGroupIndex].position.set(this.offsetX, intervalNextPosY);
            this.mainSymbolGroup[this.previousGroupIndex].position.set(this.offsetX, mainNextPosY);
        }, this);
    }

    private startRolling(){
        let perFrameOffset: number = (Math.abs(this.mainSymbolGroup[this.nextGroupIndex].position.y) + this.settings.RollBeginningOffset + this.settings.RollEndingOffset) / (this.rollingTime / 20);

        if(this.settings.RollBeginningOffset > 0){
            let intervalCurrentPosY: number = this.intervalSymbolGroup[this.previousGroupIndex].position.y;
            let mainCurrentPosY: number = this.mainSymbolGroup[this.previousGroupIndex].position.y;
            let mainTargetPosY: number = mainCurrentPosY + this.settings.RollBeginningOffset;
            
            this.rollTimerEvent = this.game.time.events.loop(20, ()=>{
                intervalCurrentPosY += perFrameOffset;
                mainCurrentPosY += perFrameOffset;
                if(mainCurrentPosY >= mainTargetPosY){
                    this.game.time.events.remove(this.rollTimerEvent);
                    this.mainSymbolGroup[this.previousGroupIndex].position.set(this.offsetX, mainTargetPosY);
                    this.generateNewIntervalSymbolGroup();
                    this.rolling(perFrameOffset);
                    return;
                }
                this.intervalSymbolGroup[this.previousGroupIndex].position.set(this.offsetX, intervalCurrentPosY);
                this.mainSymbolGroup[this.previousGroupIndex].position.set(this.offsetX, mainCurrentPosY);
            }, this);
        }else{
            this.generateNewIntervalSymbolGroup();
            this.rolling(perFrameOffset);
        }
    }

    private rolling(perFrameOffset: number){
        let intervalNextPosY: number = this.intervalSymbolGroup[this.nextGroupIndex].position.y;
        let intervalPreviousPosY: number = this.intervalSymbolGroup[this.previousGroupIndex].position.y;
        let intervalTargetPosY: number = this.offsetY - this.intervalSymbolCount * this.settings.SymbolHeight + this.settings.RollEndingOffset;
        let mainNextPosY: number = this.mainSymbolGroup[this.nextGroupIndex].position.y;
        let mainTargetPosY: number = this.offsetY + this.settings.RollEndingOffset;
        let mainPreviousPosY: number = this.mainSymbolGroup[this.previousGroupIndex].position.y;

        this.rollTimerEvent = this.game.time.events.loop(20, ()=>{
            intervalNextPosY += perFrameOffset;
            intervalPreviousPosY += perFrameOffset;
            mainNextPosY += perFrameOffset;
            mainPreviousPosY += perFrameOffset;
            if(mainNextPosY >= mainTargetPosY){
                this.game.time.events.remove(this.rollTimerEvent);
                this.intervalSymbolGroup[this.previousGroupIndex].position.set(this.offsetX, intervalTargetPosY);
                this.mainSymbolGroup[this.nextGroupIndex].position.set(this.offsetX, mainTargetPosY);
                if(this.settings.RollEndingOffset > 0){
                    this.rollEndingOffset();
                }else{
                    this.rollingEnd();
                }
                return;
            }
            this.intervalSymbolGroup[this.nextGroupIndex].position.set(this.offsetX, intervalNextPosY);
            this.intervalSymbolGroup[this.previousGroupIndex].position.set(this.offsetX, intervalPreviousPosY);
            this.mainSymbolGroup[this.nextGroupIndex].position.set(this.offsetX, mainNextPosY);
            this.mainSymbolGroup[this.previousGroupIndex].position.set(this.offsetX, mainPreviousPosY);
        }, this);
    }

    public stopRolling(){
        let intervalTargetPosY: number = this.offsetY + this.mainSymbolCount * this.settings.SymbolHeight;
    }

    private rollEndingOffset(){
        let perFrameOffset: number = this.settings.RollEndingOffset / (this.settings.RollEndingTotalTime / 20);
        let intervalPreviousPosY: number = this.intervalSymbolGroup[this.previousGroupIndex].position.y;
        let intervalTargetPosY: number = intervalPreviousPosY - this.settings.RollEndingOffset;
        let mainNextPosY: number = this.mainSymbolGroup[this.nextGroupIndex].position.y;
        let mainTargetPosY: number = mainNextPosY - this.settings.RollEndingOffset;
        
        this.rollTimerEvent = this.game.time.events.loop(20, ()=>{
            intervalPreviousPosY -= perFrameOffset;
            mainNextPosY -= perFrameOffset;
            if(mainNextPosY <= mainTargetPosY){
                this.game.time.events.remove(this.rollTimerEvent);
                this.intervalSymbolGroup[this.previousGroupIndex].position.set(this.offsetX, intervalTargetPosY);
                this.mainSymbolGroup[this.nextGroupIndex].position.set(this.offsetX, mainTargetPosY);
                this.rollingEnd();
                return;
            }
            this.intervalSymbolGroup[this.previousGroupIndex].position.set(this.offsetX, intervalPreviousPosY);
            this.mainSymbolGroup[this.nextGroupIndex].position.set(this.offsetX, mainNextPosY);
        }, this);
    }

    private rollingEnd(){
        this.previousGroupIndex = this.nextGroupIndex;
        this.nextGroupIndex = this.nextGroupIndex == 0 ? 1 : 0;
        this.rollFinishedSignal.dispatch();
    }

    private generateNewIntervalSymbolGroup(){
        this.intervalSymbolGroup[this.previousGroupIndex].children.length = 0;
        this.intervalSymbolGroup[this.previousGroupIndex].position.set(this.offsetX, this.mainSymbolGroup[this.nextGroupIndex].position.y - this.intervalSymbolCount * this.settings.SymbolHeight);
        let randomSymbol: number[] = this.generateRandomSymbol();
        for(let i: number = 0; i < this.intervalSymbolCount; i++){
            let symbol: Phaser.Sprite = this.game.add.sprite(0, this.settings.SymbolHeight * i, 'Symbol', this.symbolNames[randomSymbol[i]], this.intervalSymbolGroup[this.previousGroupIndex]);
            symbol.anchor.set(0.5);
        }
    }

    private generateRandomSymbol(){
        let randomSymbol: number[] = new Array(this.intervalSymbolCount);
        for(let i: number = 0; i < this.intervalSymbolCount; i++){
            randomSymbol[i] = Math.floor(Math.random() * 9) + 3;
        }
        return randomSymbol;
    }
}