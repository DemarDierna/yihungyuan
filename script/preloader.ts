class Preloader extends Phaser.State{

    private languagePackName: string;
    private platformName: string;
    private server: Server;
    private connectTimeoutsTimerEvent: Phaser.TimerEvent;
    private isLoginProcess: boolean;
    private gameData: GameData;
    private errorMessage: ErrorMessage;

    constructor(){
        super();
    }

    init(languagePackName: string){
        this.languagePackName = languagePackName;
    }

    preload(){
        this.platformName = this.game.cache.getJSON('GameConfig').PlatformName;
        if(this.platformName != ""){
            this.game.load.image(this.platformName, 'assets/logo/' + this.platformName + '.png');
        }
        this.game.load.pack('loading', 'assets/' + this.languagePackName + '/pack.json');
        this.game.load.onLoadComplete.add(this.serverInitialize, this);
    }

    /**伺服器初始化 */
    private serverInitialize(){
        this.game.load.onLoadComplete.remove(this.serverInitialize, this);
        this.game.add.image(0, 0, 'LoadingBackground');
        if(this.platformName != ""){
            this.game.add.image(640, 700, this.platformName).anchor.set(0.5);
        }

        let serverIndex: number = this.game.cache.getJSON('GameConfig').ServerIndex;
        let serverURL: string = this.game.cache.getJSON('GameConfig').ServerURL;

        this.server = new Server(serverURL[serverIndex]);
        this.server.connectionSignal.add(this.connectionHandler, this);
        this.server.receiveSignal.add(this.receiveHandler, this);
        this.gameData = {};

        this.errorMessage = new ErrorMessage(this.game);
        this.add.existing(this.errorMessage);
    }

    /**
     * 伺服器連接處理程序
     * @param connectionStatus 伺服器連接狀態
     */
    private connectionHandler(connectionStatus: boolean){
        if(connectionStatus){
            //檢查網址是否有夾帶Token，是則套用，否則不帶
            let regexToken: RegExp = /token=(\w+)/;
            let matchToken: string[] = regexToken.exec(window.location.href);
            let gameID: number = this.game.cache.getJSON('GameConfig').GameID;
            let urlToken: string = matchToken != undefined ? matchToken[1] : "";
            
            let packet: Object = {
                Code: PACKET_CODE.CtoGJoinGame,
                GameToken: urlToken,
                GameID: gameID
            };

            this.connectTimeoutsTimerEvent = this.game.time.events.add(10000, () => {
                this.server.close();
                this.errorMessage.showError();
            }, this);

            this.isLoginProcess = true;
            this.server.send(JSON.stringify(packet));
        }else{
            this.game.time.events.remove(this.connectTimeoutsTimerEvent);
            this.errorMessage.showError(this.isLoginProcess ? "Token Invalid" : undefined);
        }
    }

    /**
     * 伺服器資料處理程序
     * @param data 伺服器傳來的資料
     */
    private receiveHandler(data: string){
        let json = JSON.parse(data);
        switch(json.Code){
            case PACKET_CODE.GtoCGameError:
                // this.errorMessage.showError(json.packet.errorMessage);
                break;
            case PACKET_CODE.GtoCJoinGame:
                this.game.time.events.remove(this.connectTimeoutsTimerEvent);
                this.isLoginProcess = false;
                this.gameData.money = json.Money;
                break;
            case PACKET_CODE.GtoCSlotInit:
                this.gameData.moneyFractionMultiple = json.MoneyFractionMultiple;
                this.gameData.denom = json.Denom / 100;
                this.gameData.line = json.Line;
                this.gameData.betMultiples = json.BetMultiples;
                this.gameData.betUnit = json.BetUnit;

                this.server.connectionSignal.remove(this.connectionHandler);
                this.server.receiveSignal.remove(this.receiveHandler);
                
                this.game.state.add("GameLoader", GameLoader);
                this.game.state.start("GameLoader", true, false, this.languagePackName, this.platformName, this.server, this.gameData);
                break;
        }
    }
}