class GameLoader extends Phaser.State{

    private server: Server;
    private gameData: GameData;
    private languagePackName: string;
    private loadingBar: Phaser.Graphics;
    private errorMessage: ErrorMessage;

    constructor(){
        super();
    }

    init(languagePackName: string, platformName: string, server: Server, gameData: GameData){
        this.languagePackName = languagePackName;
        this.gameData = gameData;
        this.server = server;
        this.server.connectionSignal.add(this.connectionHandler, this);
        this.server.receiveSignal.add(this.receiveHandler, this);

        this.game.add.image(0, 0, 'LoadingBackground');
        if(platformName != ""){
            this.game.add.image(640, 700, platformName).anchor.set(0.5);
        }
        let loadingBarBG: Phaser.Graphics = this.game.add.graphics(0, 755);
        loadingBarBG.beginFill(0xc47300);
        loadingBarBG.drawRect(0, 0, 1280, 3);
        loadingBarBG.endFill();

        this.loadingBar = this.game.add.graphics(0, 755);
        this.loadingBar.beginFill(0xffff00);
        this.loadingBar.drawRect(0, 0, 1280, 3);
        this.loadingBar.endFill();
        this.loadingBar.scale.set(0, 1);

        this.errorMessage = new ErrorMessage(this.game);
        this.add.existing(this.errorMessage);
    }

    preload(){
        this.game.load.pack('ui', 'assets/generic/pack.json');
        this.game.load.pack('game', 'assets/generic/pack.json');
        this.game.load.pack('ui', 'assets/'+ this.languagePackName +'/pack.json');
        this.game.load.pack('game', 'assets/'+ this.languagePackName +'/pack.json');
        this.game.load.onFileComplete.add(this.fileComplete, this);
        this.game.load.onLoadComplete.add(this.loadCompleted, this);
    }

    /**
     * 檔案加載處理程序
     * @param progress 加載進度(0~100)
     * @param cacheKey 
     * @param success 
     * @param totalLoaded 
     * @param totalFiles 
     */
    private fileComplete(progress, cacheKey, success, totalLoaded, totalFiles) {
        this.loadingBar.scale.set(progress / 100, 1);
    }

    /**
     * 全部資源加載處理完畢後執行
     */
    private loadCompleted(){
        this.game.load.onFileComplete.remove(this.fileComplete, this);
        this.game.load.onLoadComplete.remove(this.loadCompleted, this);

        this.server.connectionSignal.remove(this.connectionHandler);
        this.server.receiveSignal.remove(this.receiveHandler);

        this.game.state.add('GameManager', GameManager);
        this.game.state.start('GameManager', true, false, this.server, this.gameData);
    }

    /**
     * 伺服器連接處理程序
     * @param connectionStatus 伺服器連接狀態
     */
    private connectionHandler(connectionStatus: boolean){
        if(!connectionStatus){
            this.errorMessage.showError();
        }
    }

    /**
     * 伺服器資料處理程序
     * @param data 伺服器傳來的資料
     */
    private receiveHandler(data: string){
        let json = JSON.parse(data);
        switch(json.Code){
            case PACKET_CODE.GtoCGameError:
                this.errorMessage.showError(json.packet.errorMessage);
                this.server.close();
                break;
        }
    }
}