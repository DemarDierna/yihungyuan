class Server{
    public receiveSignal: Phaser.Signal = new Phaser.Signal();
    public connectionSignal: Phaser.Signal = new Phaser.Signal();
    private websocket: WebSocket;

    constructor(url: string) {
        this.connect(url);
    }

    private connect(url: string) {
        let self = this;
        this.websocket = new WebSocket(url);
        
        this.websocket.onopen = (event: any)=>{
            self.connectionSignal.dispatch(true);
        }

        this.websocket.onmessage = (event: any)=>{
            self.receiveSignal.dispatch(event.data);
        }

        this.websocket.onclose = (event: any)=>{
            self.connectionSignal.dispatch(false);
        }

        this.websocket.onerror = (event: any)=>{
            console.error(event);
        }
    }

    public close() {
        this.websocket.close();
    }

    public send(message: string) {
        this.websocket.send(message);
    }
}