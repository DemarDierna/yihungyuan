/**封包種類 */
enum PACKET_CODE{
    GtoCGameError = 1001,
    CtoGJoinGame = 1002,
    GtoCJoinGame = 1005,
    GtoCSlotInit = 11001,
    CtoGSlotNGPlay = 11002,
    GtoCSlotNGPlay = 11003
}

/**旋轉選單的狀態 */
enum SPIN_STATUS{
    IDLE = 0,
    START,
    STOP,
    AUTO,
    AUTO_FAST,
    WAIT_FOR_CREDITING,
    WAIT_FOR_TAKE_WIN,
    TAKE_WIN
}

/**滾輪的狀態 */
enum REEL_STATUS{
    IDLE = 0,
    SPIN,
    ROLLING,
    FINISHED,
    SCORING,
    ENDING
}