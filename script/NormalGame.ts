class NormalGame extends BasicGameMod{

    private menuOpen: Phaser.Button;
    private insufficientBalanceGroup: Phaser.Group;

    public initGame(){
        this.game.add.image(188, 134, 'NormalGameBackgroundBottom', null, this);
        this.initReels();
        this.game.add.image(0, 0, 'NormalGameBackground', null, this);
        
        this.initDataDisplayPanel();
        this.initSpinControlPanel();
        this.spinStatusChangedSignal.add((newStatus: SPIN_STATUS)=>{
            this.spinStatusChanged(newStatus);
        }, this);
        this.initMenuPanel();
        this.initInsufficientBalancePanel();
    }

    /**初始化選單面板 */
    public initMenuPanel(){
        this.menuOpen = this.game.add.button(60, 708, 'UI', null, this, 'Menu_Over', 'Menu_Up', 'Menu_Down', 'Menu_Up', this);
        this.menuOpen.anchor.set(0.5);
        this.menuOpen.onInputUp.add(()=>{
            this.menuOpen.visible = false;
            menuGroup.visible = true;
        }, this);

        let menuGroup: Phaser.Group = this.game.add.group(this);
        menuGroup.visible = false;

        let mask: Phaser.Graphics = this.game.add.graphics(0, 0, menuGroup);
        mask.beginFill(0x000000, 0.6);
        mask.drawRect(0, 0, 1280, 768);
        mask.endFill();
        mask.inputEnabled = true;
        mask.events.onInputUp.add(()=>{
            menuGroup.visible = false;
            this.menuOpen.visible = true;
        }, menuGroup);

        let gameConfig: any = this.game.cache.getJSON('GameConfig');
        this.game.add.sprite(0, 768, 'UI', 'Menu_BG_' + gameConfig.NumberOfMenuColumn, menuGroup).anchor.set(0, 1);

        let menuClose: Phaser.Button = this.game.add.button(60, 708, 'UI', null, this, 'MenuClose_Over', 'MenuClose_Up', 'MenuClose_Down', 'MenuClose_Up', menuGroup);
        menuClose.anchor.set(0.5);
        menuClose.onInputUp.add(()=>{
            menuGroup.visible = false;
            this.menuOpen.visible = true;
        }, menuGroup);

        let instructionPageOpen: Phaser.Button = this.game.add.button(60, 594, 'UI', null, this, 'InstructionPage_Over', 'InstructionPage_Up', 'InstructionPage_Down', 'InstructionPage_Up', menuGroup);
        instructionPageOpen.anchor.set(0.5);
        instructionPageOpen.onInputUp.add(()=>{
            instructionPageGroup.visible = true;
        }, menuGroup);

        let soundOpen: Phaser.Button = this.game.add.button(60, 480, 'UI', null, this, 'SoundOpen_Over', 'SoundOpen_Up', 'SoundOpen_Down', 'SoundOpen_Up', menuGroup);
        soundOpen.anchor.set(0.5);
        soundOpen.onInputUp.add(()=>{
            soundOpen.visible = false;
            soundClose.visible = true;
        }, menuGroup);

        let soundClose: Phaser.Button = this.game.add.button(60, 480, 'UI', null, this, 'SoundClose_Over', 'SoundClose_Up', 'SoundClose_Down', 'SoundClose_Up', menuGroup);
        soundClose.anchor.set(0.5);
        soundClose.visible = false;
        soundClose.onInputUp.add(()=>{
            soundClose.visible = false;
            soundOpen.visible = true;
        }, menuGroup);

        if(gameConfig.NumberOfMenuColumn == 5){
            let historicalRecordOpen: Phaser.Button = this.game.add.button(60, 366, 'UI', null, this, 'HistoricalRecord_Over', 'HistoricalRecord_Up', 'HistoricalRecord_Down', 'HistoricalRecord_Up', menuGroup);
            historicalRecordOpen.anchor.set(0.5);

            let exit: Phaser.Button = this.game.add.button(60, 252, 'UI', null, this, 'Exit_Over', 'Exit_Up', 'Exit_Down', 'Exit_Up', menuGroup);
            exit.anchor.set(0.5);
        }

        let instructionPageGroup: Phaser.Group = this.game.add.group(this);
        instructionPageGroup.visible = false;
        this.initInstructionPage(gameConfig.NumberOfInstructionPages, instructionPageGroup);
    }

    /**
     * 初始化說明頁
     * @param numberOfPages 總頁數
     * @param pageGroup 說明頁的群組
     */
    private initInstructionPage(numberOfPages: number, pageGroup: Phaser.Group){
        let currentPage: number = 0;
        let previousPage: number = 0;
        let pages: Phaser.Image[] = new Array(numberOfPages);
        for(let i: number = 0; i < numberOfPages; i++){
            pages[i] = this.game.add.image(0, 0, 'InstructionPage_' + (i + 1), null, pageGroup);
            pages[i].visible = i == currentPage ? true : false;
            pages[i].inputEnabled = true;
            pages[i].events.onInputUp.add(()=>{
                currentPage = currentPage < numberOfPages - 1 ? currentPage + 1 : 0;
                changePage();
            }, pageGroup);
        }

        let pageClose: Phaser.Button = this.game.add.button(1220, 0, 'UI', null, this, 'InstructionPage_Close', 'InstructionPage_Close', 'InstructionPage_Close', 'InstructionPage_Close', pageGroup);
        pageClose.onInputDown.add(()=>{
            pageClose.tint = 0x949494;
        }, pageGroup);
        pageClose.onInputUp.add(()=>{
            pageClose.tint = 0xffffff;
            pageGroup.visible = false;
        }, pageGroup);

        let pagePrevious: Phaser.Button = this.game.add.button(0, 384, 'UI', null, this, 'InstructionPage_Previous', 'InstructionPage_Previous', 'InstructionPage_Previous', 'InstructionPage_Previous', pageGroup);
        pagePrevious.anchor.set(0, 0.5);
        pagePrevious.onInputDown.add(()=>{
            pagePrevious.tint = 0x949494;
        }, pageGroup);
        pagePrevious.onInputUp.add(()=>{
            pagePrevious.tint = 0xffffff;
            currentPage = currentPage > 0 ? currentPage - 1 : numberOfPages - 1;
            changePage();
        }, pageGroup);

        let pageNext: Phaser.Button = this.game.add.button(1280, 384, 'UI', null, this, 'InstructionPage_Next', 'InstructionPage_Next', 'InstructionPage_Next', 'InstructionPage_Next', pageGroup);
        pageNext.anchor.set(1, 0.5);
        pageNext.onInputDown.add(()=>{
            pageNext.tint = 0x949494;
        }, pageGroup);
        pageNext.onInputUp.add(()=>{
            pageNext.tint = 0xffffff;
            currentPage = currentPage < numberOfPages - 1 ? currentPage + 1 : 0;
            changePage();
        }, pageGroup);

        /**切換頁面 */
        function changePage(){
            pages[currentPage].visible = true;
            pages[previousPage].visible = false;
            previousPage = currentPage;
        }
    }

    private spinStatusChanged(newStatus: SPIN_STATUS){
        switch(newStatus){
            case SPIN_STATUS.START:
                this.changeReelStatus(REEL_STATUS.SPIN);
                break;
        }
    }

    public changeReelStatus(newStatus: REEL_STATUS, data?: any){
        switch(newStatus){
            case REEL_STATUS.IDLE:
                break;
            case REEL_STATUS.SPIN:
                let currentTotalBet: number = this.gameData.line * this.gameData.betUnit * this.gameData.betMultiples[this.betMultiple];
                let currentCredits: number = this.gameData.money / this.gameData.denom / this.gameData.moneyFractionMultiple;
                if(currentCredits >= currentTotalBet){
                    let packet: Object = {
                        Code: PACKET_CODE.CtoGSlotNGPlay,
                        BetMultiple: this.betMultiple
                    };
                    this.sendMessageSignal.dispatch(JSON.stringify(packet));
                }else{
                    this.changeSpinStatus(SPIN_STATUS.IDLE);
                    this.insufficientBalanceGroup.visible = true;
                }
                break;
            case REEL_STATUS.ROLLING:
                this.roundIdText.setText(data.RoundID + "");
                break;
        }
    }

    /**初始化餘額不足面板 */
    private initInsufficientBalancePanel(){
        this.insufficientBalanceGroup = this.game.add.group(this);
        this.insufficientBalanceGroup.visible = false;

        let mask: Phaser.Graphics = this.game.add.graphics(0, 0, this.insufficientBalanceGroup);
        mask.beginFill(0x000000, 0.6);
        mask.drawRect(0, 0, 1280, 768);
        mask.endFill();
        mask.inputEnabled = true;

        let languageSettings: any = this.game.cache.getJSON('LanguageSettings');
        this.game.add.sprite(640, 384, 'UI', 'InsufficientBalance_BG', this.insufficientBalanceGroup).anchor.set(0.5);
        this.game.add.text(640, 340, languageSettings.InsufficientBalanceTitle, {font: "bold 60px Microsoft JhengHei", fill: "#fff"}, this.insufficientBalanceGroup).anchor.set(0.5);
        let confirm: Phaser.Image = this.game.add.image(640, 450, 'InsufficientBalanceConfirm', null, this.insufficientBalanceGroup);
        confirm.anchor.set(0.5);
        confirm.inputEnabled = true;
        confirm.events.onInputDown.add(()=>{
            confirm.tint = 0x949494;
        }, this.insufficientBalanceGroup);
        confirm.events.onInputUp.add(()=>{
            confirm.tint = 0xffffff;
            this.insufficientBalanceGroup.visible = false;
        }, this.insufficientBalanceGroup);
    }
}