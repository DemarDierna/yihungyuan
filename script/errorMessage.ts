class ErrorMessage extends Phaser.Group{
    private errorMessage: Phaser.Text;

    constructor(game: Phaser.Game){
        super(game);
        this.initialize();
    }

    /**頁面初始化 */
    private initialize(){
        let errorMask: Phaser.Graphics = this.game.add.graphics(0, 0, this);
        errorMask.beginFill(0x000000, 1);
        errorMask.drawRect(0, 0, 1280, 768);
        errorMask.endFill();
        errorMask.inputEnabled = true;
        
        this.errorMessage = this.game.add.text(640, 384, "", {font: "bold 56px Arial", fill: "#ffffff", align: "center", wordWrap: true, wordWrapWidth: 1200 }, this);
        this.errorMessage.anchor.setTo(0.5);

        this.visible = false;
    }

    /**顯示錯誤頁面，可帶入要顯示的錯誤訊息，預設為連線關閉
     * @param errorMessage 錯誤訊息
     */
    public showError(errorMessage?: string){
        if(this.visible){
            return;
        }
        this.game.input.keyboard.removeKey(Phaser.Keyboard.SPACEBAR);
        this.game.time.events.removeAll();
        this.errorMessage.setText(errorMessage != undefined ? errorMessage : "Connect Close");
        this.visible = true;
    }
}