class BasicGameMod extends Phaser.Group{

    public sendMessageSignal: Phaser.Signal;
    public gameData: GameData;
    private creditsText: Phaser.Text;
    private win: number;
    private winText: Phaser.Text;
    public betMultiple: number;
    private totalBetText: Phaser.Text;
    public roundIdText: Phaser.Text;
    private betMultipleText: Phaser.Text;
    private displayCurrency: boolean;
    private reels: Reel[];
    private reelFinishedCount: number;

    public spinStatusChangedSignal: Phaser.Signal;
    private keySpace: Phaser.Key;
    private skipMask: Phaser.Graphics;
    private spinStart: Phaser.Button;
    private spinStop: Phaser.Button;
    private takeWin: Phaser.Button;
    private betAdd: Phaser.Button;
    private betReduce: Phaser.Button;
    private autoSpin: Phaser.Button;
    private autoFastSpin: Phaser.Button;
    private spinMenuOpen: Phaser.Button;
    private spinMenuClose: Phaser.Button;

    constructor(game: Phaser.Game, gameData: GameData){
        super(game);
        this.gameData = gameData;
        this.sendMessageSignal = new Phaser.Signal();
        this.initGame();
    }

    /**初始化遊戲 */
    public initGame(){

    }

    /**初始化資料顯示面板 */
    public initDataDisplayPanel(){
        let languageSettings: any = this.game.cache.getJSON('LanguageSettings');
        
        this.displayCurrency = true;
        this.win = 0;
        this.betMultiple = 0;

        this.game.add.image(0, 768, 'UIBase', null, this).anchor.set(0, 1);
        
        this.creditsText = this.game.add.text(280, 720, "", { font: "bold 36px Microsoft JhengHei", fill: "#ffffff" }, this);
        this.creditsText.anchor.set(0.5);
        this.creditsText.inputEnabled = true;
        this.creditsText.events.onInputUp.add(this.changeDisplayCurrency, this);

        this.winText = this.game.add.text(640, 720, "", { font: "bold 36px Microsoft JhengHei", fill: "#ffffff" }, this);
        this.winText.anchor.set(0.5);
        this.winText.inputEnabled = true;
        this.winText.events.onInputUp.add(this.changeDisplayCurrency, this);

        this.totalBetText = this.game.add.text(1045, 720, "", { font: "bold 36px Microsoft JhengHei", fill: "#ffffff" }, this);
        this.totalBetText.anchor.set(0.5);
        this.totalBetText.inputEnabled = true;
        this.totalBetText.events.onInputUp.add(this.changeDisplayCurrency, this);

        this.roundIdText = this.game.add.text(150, 755, "", { font: "bold 20px Microsoft JhengHei", fill: "#ffffff" }, this);
        this.roundIdText.anchor.set(0, 0.5);
        
        this.game.add.text(1060, 755, languageSettings.BetLevelTitle, { font: "bold 20px Microsoft JhengHei", fill: "#ffffff" }, this).anchor.set(1, 0.5);
        this.betMultipleText = this.game.add.text(1070, 755, "X" + this.gameData.betMultiples[this.betMultiple], { font: "bold 20px Microsoft JhengHei", fill: "#ffffff" }, this);
        this.betMultipleText.anchor.set(0, 0.5);
        
        this.updateDataDisplayBoard();
    }

    /**初始化旋轉控制面板 */
    public initSpinControlPanel(){
        this.skipMask = this.game.add.graphics(188, 134, this);
        this.skipMask.beginFill(0xffffff, 0);
        this.skipMask.drawRect(0, 0, 1004, 504);
        this.skipMask.endFill();
        this.skipMask.inputEnabled = true;
        this.skipMask.input.useHandCursor = true;

        this.spinStart = this.game.add.button(1210, 384, 'UI', null, this, 'SpinStart_Over', 'SpinStart_Up', 'SpinStart_Down', 'SpinStart_Up', this);
        this.spinStart.anchor.set(0.5);
        this.spinStart.events.onInputUp.add(()=>{
            this.changeSpinStatus(SPIN_STATUS.START);
        }, this);

        this.spinStop = this.game.add.button(1210, 384, 'UI', null, this, 'SpinStop_Over', 'SpinStop_Up', 'SpinStop_Down', 'SpinStop_Up', this);
        this.spinStop.anchor.set(0.5);
        this.spinStop.events.onInputUp.add(()=>{
            this.changeSpinStatus(SPIN_STATUS.STOP);
        }, this);

        this.takeWin = this.game.add.button(1210, 384, 'UI', null, this, 'TakeWin_Over', 'TakeWin_Up', 'TakeWin_Down', 'TakeWin_Up', this);
        this.takeWin.anchor.set(0.5);

        let betLongClickTimerEvent: Phaser.TimerEvent;
        let betLongClickStatus: boolean = false;
        let betMultiplesMax: number = this.gameData.betMultiples.length - 1;
        this.betAdd = this.game.add.button(1210, 274, 'UI', null, this, 'BetAdd_Over', 'BetAdd_Up', 'BetAdd_Down', 'BetAdd_Up', this);
        this.betAdd.anchor.set(0.5);
        this.betAdd.events.onInputDown.add(()=>{
            betLongClickTimerEvent = this.game.time.events.add(1000, startLongClickBet, this, true);
        }, this);
        this.betAdd.events.onInputUp.add(()=>{
            this.game.time.events.remove(betLongClickTimerEvent);
            if(!betLongClickStatus){
                this.betMultiple = this.betMultiple < betMultiplesMax ? this.betMultiple + 1 : 0;
                this.updateDataDisplayBoard();
            }else{
                betLongClickStatus = false;
            }
        }, this);

        this.betReduce = this.game.add.button(1210, 494, 'UI', null, this, 'BetReduce_Over', 'BetReduce_Up', 'BetReduce_Down', 'BetReduce_Up', this);
        this.betReduce.anchor.set(0.5);
        this.betReduce.events.onInputDown.add(()=>{
            betLongClickTimerEvent = this.game.time.events.add(1000, startLongClickBet, this, false);
        }, this);
        this.betReduce.events.onInputUp.add(()=>{
            this.game.time.events.remove(betLongClickTimerEvent);
            if(!betLongClickStatus){
                this.betMultiple = this.betMultiple > 0 ? this.betMultiple - 1 : betMultiplesMax;
                this.updateDataDisplayBoard();
            }else{
                betLongClickStatus = false;
            }
        }, this);

        /**
         * 開始長按押注
         * @param betStatus 押注狀態(true=增加／false=減少)
         */
        function startLongClickBet(betStatus: boolean){
            betLongClickStatus = true;
            this.game.time.events.remove(betLongClickTimerEvent);
            betLongClickTimerEvent = this.game.time.events.loop(100, ()=>{
                this.betMultiple = betStatus ? this.betMultiple < betMultiplesMax ? this.betMultiple + 1 : this.betMultiple
                                             : this.betMultiple > 0 ? this.betMultiple - 1 : this.betMultiple;
                this.updateDataDisplayBoard();
            }, this);
        }

        this.autoSpin = this.game.add.button(1210, 274, 'UI', null, this, 'AutoSpin_Over', 'AutoSpin_Up', 'AutoSpin_Down', 'AutoSpin_Up', this);
        this.autoSpin.anchor.set(0.5);

        this.autoFastSpin = this.game.add.button(1210, 494, 'UI', null, this, 'AutoFastSpin_Over', 'AutoFastSpin_Up', 'AutoFastSpin_Down', 'AutoFastSpin_Up', this);
        this.autoFastSpin.anchor.set(0.5);

        this.spinMenuOpen = this.game.add.button(1210, 590, 'UI', null, this, 'SpinMenuOpen_Over', 'SpinMenuOpen_Up', 'SpinMenuOpen_Down', 'SpinMenuOpen_Up', this);
        this.spinMenuOpen.anchor.set(0.5);
        this.spinMenuOpen.events.onInputUp.add(()=>{
            this.switchAutoSpinMenu(true);
        }, this);

        this.spinMenuClose = this.game.add.button(1210, 590, 'UI', null, this, 'SpinMenuClose_Over', 'SpinMenuClose_Up', 'SpinMenuClose_Down', 'SpinMenuClose_Up', this);
        this.spinMenuClose.anchor.set(0.5);
        this.spinMenuClose.events.onInputUp.add(()=>{
            this.switchAutoSpinMenu(false);
        }, this);

        this.spinStatusChangedSignal = new Phaser.Signal();
        this.keySpace = this.game.input.keyboard.addKey(Phaser.KeyCode.SPACEBAR);

        this.switchAutoSpinMenu(false);
        this.changeSpinStatus(SPIN_STATUS.IDLE);
    }

    /**
     * 改變旋轉狀態
     * @param newStatus 新的旋轉狀態
     */
    public changeSpinStatus(newStatus: SPIN_STATUS){
        this.keySpace.onUp.removeAll();
        this.skipMask.events.onInputUp.removeAll();

        switch(newStatus){
            case SPIN_STATUS.IDLE:
                this.spinStart.visible = this.betAdd.visible = this.betReduce.visible = this.spinMenuOpen.visible = true;
                this.spinStop.visible = this.takeWin.visible = false;

                this.keySpace.onUp.addOnce(()=>{
                    this.changeSpinStatus(SPIN_STATUS.START);
                }, this);
                break;
            case SPIN_STATUS.START:
                this.spinStart.visible = this.betAdd.visible = this.betReduce.visible = this.autoSpin.visible = this.autoFastSpin.visible = this.spinMenuOpen.visible = this.spinMenuClose.visible = false;
                this.spinStop.visible = this.spinStop.inputEnabled = true;
                break;
            case SPIN_STATUS.STOP:
                
                break;
        }
        this.spinStatusChangedSignal.dispatch(newStatus);
    }

    /**
     * 開關自動滾輪選單
     * @param status 選單開啟狀態
     */
    private switchAutoSpinMenu(status: boolean){
        this.spinMenuOpen.visible = !status;
        this.spinMenuClose.visible = status;
        this.betAdd.visible = !status;
        this.betReduce.visible = !status;
        this.autoSpin.visible = status;
        this.autoFastSpin.visible = status;
    }

    /**更改顯示幣別 */
    private changeDisplayCurrency(){
        this.displayCurrency = !this.displayCurrency;
        this.updateDataDisplayBoard();
    }

    /**更新資料顯示面板 */
    private updateDataDisplayBoard(){
        if(this.displayCurrency){
            this.creditsText.setText("¥" + this.floatWithCommas(this.gameData.money / this.gameData.moneyFractionMultiple));
            this.winText.setText("¥" + this.floatWithCommas(this.win / this.gameData.moneyFractionMultiple));
            this.totalBetText.setText("¥" + this.numberWithCommas((this.gameData.line * this.gameData.betUnit * this.gameData.betMultiples[this.betMultiple]) / this.gameData.moneyFractionMultiple));
        }else{
            this.creditsText.setText(this.numberWithCommas(Math.floor(this.gameData.money / this.gameData.denom / this.gameData.moneyFractionMultiple)));
            this.winText.setText(this.numberWithCommas(Math.floor(this.win / this.gameData.denom / this.gameData.moneyFractionMultiple)));
            this.totalBetText.setText(this.numberWithCommas(this.gameData.line * this.gameData.betUnit * this.gameData.betMultiples[this.betMultiple]));
        }
        this.betMultipleText.setText("X" + this.gameData.betMultiples[this.betMultiple]);
    }

    /**
     * 將帶入的數值轉換成帶有逗點的字串並回傳
     * @param value 數值
     */
    private numberWithCommas(value: number){
        return Number(value).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    /**
     * 將帶入的數值轉換成帶有逗點及小數點後二位的字串並回傳
     * @param value 數值
     */
    private floatWithCommas(value: number){
        let parts: string[] = value.toString().split(".");
        parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        if (parts.length > 1) {
            parts[1] = parts[1].substring(0, 2);
            if (parts[1].length == 1) {
                parts[1] += "0";
            }
        } else {
            parts[1] = "00"
        }
        return parts.join(".");
    }

    /**
     * 初始化滾輪
     */
    public initReels(){
        let reelSettings: any = this.game.cache.getJSON('ReelSettings');
        this.reels = new Array(reelSettings.Strip);
        for(let i: number = 0; i < reelSettings.Strip; i++){
            this.reels[i] = new Reel(this.game);
            this.reels[i].init(i, reelSettings);
            this.reels[i].rollFinishedSignal.add(()=>{
                this.reelRollFinished(i);
            }, this);
            this.add(this.reels[i]);
        }
    }

    public sendSpinData(symbolResult: number[][]){
        this.reelFinishedCount = 0;
        for(let i: number = 0; i < symbolResult.length; i++){
            this.reels[i].prepareToRolling(symbolResult[i]);
        }
    }

    private reelRollFinished(stripIndex: number){
        this.reelFinishedCount++;
        if(this.reelFinishedCount == this.reels.length){
            console.log("finis");
        }
    }
}