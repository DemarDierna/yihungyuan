class GameEntry{
	
    private game: Phaser.Game;
	
	constructor(){
        this.game = new Phaser.Game(1280, 768, Phaser.AUTO, "", {
            preload: this.preload,
            create: this.create
        });
    }
    
    preload(){
        this.game.load.json('GameConfig', 'gameConfig.json');
    }

	create(){
        this.game.stage.disableVisibilityChange = true;
        this.game.scale.pageAlignHorizontally = true;
        this.game.scale.pageAlignVertically = true;
        
		if (this.game.device.android) {
            this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
        } else {
            this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        }

        let gameConfig: any = this.game.cache.getJSON('GameConfig');
        
        let regexLanguage: RegExp = /language=([^&]+)/;
        let matchLanguage: string[] = regexLanguage.exec(window.location.href);
        let allLanguages: string[] = gameConfig.LanguageSupport;
        
        //檢查Url夾帶的語系是否是合法的，是則套用，否則預設為en
        let urlLanguage: string = matchLanguage != undefined ? (allLanguages.find((value, index, array)=>{
            return matchLanguage[1] == value;
        }) ? matchLanguage[1] : allLanguages[0]) : allLanguages[0];

        let languageID: number = allLanguages.findIndex(languageID => languageID === urlLanguage);
        let languagePackName: string = gameConfig.LanguagePackName[languageID];
        document.title = gameConfig.GameTitle[languageID];

        this.game.state.add('Preloader', Preloader);
        this.game.state.start('Preloader', true, false, languagePackName);
    }
    
    
}
new GameEntry();