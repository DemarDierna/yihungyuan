class FreeGame extends BasicGameMod{
    
    public initGame(){
        this.game.add.image(188, 134, 'FreeGameBackgroundBottom', null, this);
        this.game.add.image(0, 0, 'FreeGameBackground', null, this);
        this.initDataDisplayPanel();
    }
}