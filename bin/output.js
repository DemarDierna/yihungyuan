class BasicGameMod extends Phaser.Group {
    constructor(game, gameData) {
        super(game);
        this.gameData = gameData;
        this.sendMessageSignal = new Phaser.Signal();
        this.initGame();
    }
    initGame() {
    }
    initDataDisplayPanel() {
        let languageSettings = this.game.cache.getJSON('LanguageSettings');
        this.displayCurrency = true;
        this.win = 0;
        this.betMultiple = 0;
        this.game.add.image(0, 768, 'UIBase', null, this).anchor.set(0, 1);
        this.creditsText = this.game.add.text(280, 720, "", { font: "bold 36px Microsoft JhengHei", fill: "#ffffff" }, this);
        this.creditsText.anchor.set(0.5);
        this.creditsText.inputEnabled = true;
        this.creditsText.events.onInputUp.add(this.changeDisplayCurrency, this);
        this.winText = this.game.add.text(640, 720, "", { font: "bold 36px Microsoft JhengHei", fill: "#ffffff" }, this);
        this.winText.anchor.set(0.5);
        this.winText.inputEnabled = true;
        this.winText.events.onInputUp.add(this.changeDisplayCurrency, this);
        this.totalBetText = this.game.add.text(1045, 720, "", { font: "bold 36px Microsoft JhengHei", fill: "#ffffff" }, this);
        this.totalBetText.anchor.set(0.5);
        this.totalBetText.inputEnabled = true;
        this.totalBetText.events.onInputUp.add(this.changeDisplayCurrency, this);
        this.roundIdText = this.game.add.text(150, 755, "", { font: "bold 20px Microsoft JhengHei", fill: "#ffffff" }, this);
        this.roundIdText.anchor.set(0, 0.5);
        this.game.add.text(1060, 755, languageSettings.BetLevelTitle, { font: "bold 20px Microsoft JhengHei", fill: "#ffffff" }, this).anchor.set(1, 0.5);
        this.betMultipleText = this.game.add.text(1070, 755, "X" + this.gameData.betMultiples[this.betMultiple], { font: "bold 20px Microsoft JhengHei", fill: "#ffffff" }, this);
        this.betMultipleText.anchor.set(0, 0.5);
        this.updateDataDisplayBoard();
    }
    initSpinControlPanel() {
        this.skipMask = this.game.add.graphics(188, 134, this);
        this.skipMask.beginFill(0xffffff, 0);
        this.skipMask.drawRect(0, 0, 1004, 504);
        this.skipMask.endFill();
        this.skipMask.inputEnabled = true;
        this.skipMask.input.useHandCursor = true;
        this.spinStart = this.game.add.button(1210, 384, 'UI', null, this, 'SpinStart_Over', 'SpinStart_Up', 'SpinStart_Down', 'SpinStart_Up', this);
        this.spinStart.anchor.set(0.5);
        this.spinStart.events.onInputUp.add(() => {
            this.changeSpinStatus(SPIN_STATUS.START);
        }, this);
        this.spinStop = this.game.add.button(1210, 384, 'UI', null, this, 'SpinStop_Over', 'SpinStop_Up', 'SpinStop_Down', 'SpinStop_Up', this);
        this.spinStop.anchor.set(0.5);
        this.spinStop.events.onInputUp.add(() => {
            this.changeSpinStatus(SPIN_STATUS.STOP);
        }, this);
        this.takeWin = this.game.add.button(1210, 384, 'UI', null, this, 'TakeWin_Over', 'TakeWin_Up', 'TakeWin_Down', 'TakeWin_Up', this);
        this.takeWin.anchor.set(0.5);
        let betLongClickTimerEvent;
        let betLongClickStatus = false;
        let betMultiplesMax = this.gameData.betMultiples.length - 1;
        this.betAdd = this.game.add.button(1210, 274, 'UI', null, this, 'BetAdd_Over', 'BetAdd_Up', 'BetAdd_Down', 'BetAdd_Up', this);
        this.betAdd.anchor.set(0.5);
        this.betAdd.events.onInputDown.add(() => {
            betLongClickTimerEvent = this.game.time.events.add(1000, startLongClickBet, this, true);
        }, this);
        this.betAdd.events.onInputUp.add(() => {
            this.game.time.events.remove(betLongClickTimerEvent);
            if (!betLongClickStatus) {
                this.betMultiple = this.betMultiple < betMultiplesMax ? this.betMultiple + 1 : 0;
                this.updateDataDisplayBoard();
            }
            else {
                betLongClickStatus = false;
            }
        }, this);
        this.betReduce = this.game.add.button(1210, 494, 'UI', null, this, 'BetReduce_Over', 'BetReduce_Up', 'BetReduce_Down', 'BetReduce_Up', this);
        this.betReduce.anchor.set(0.5);
        this.betReduce.events.onInputDown.add(() => {
            betLongClickTimerEvent = this.game.time.events.add(1000, startLongClickBet, this, false);
        }, this);
        this.betReduce.events.onInputUp.add(() => {
            this.game.time.events.remove(betLongClickTimerEvent);
            if (!betLongClickStatus) {
                this.betMultiple = this.betMultiple > 0 ? this.betMultiple - 1 : betMultiplesMax;
                this.updateDataDisplayBoard();
            }
            else {
                betLongClickStatus = false;
            }
        }, this);
        function startLongClickBet(betStatus) {
            betLongClickStatus = true;
            this.game.time.events.remove(betLongClickTimerEvent);
            betLongClickTimerEvent = this.game.time.events.loop(100, () => {
                this.betMultiple = betStatus ? this.betMultiple < betMultiplesMax ? this.betMultiple + 1 : this.betMultiple
                    : this.betMultiple > 0 ? this.betMultiple - 1 : this.betMultiple;
                this.updateDataDisplayBoard();
            }, this);
        }
        this.autoSpin = this.game.add.button(1210, 274, 'UI', null, this, 'AutoSpin_Over', 'AutoSpin_Up', 'AutoSpin_Down', 'AutoSpin_Up', this);
        this.autoSpin.anchor.set(0.5);
        this.autoFastSpin = this.game.add.button(1210, 494, 'UI', null, this, 'AutoFastSpin_Over', 'AutoFastSpin_Up', 'AutoFastSpin_Down', 'AutoFastSpin_Up', this);
        this.autoFastSpin.anchor.set(0.5);
        this.spinMenuOpen = this.game.add.button(1210, 590, 'UI', null, this, 'SpinMenuOpen_Over', 'SpinMenuOpen_Up', 'SpinMenuOpen_Down', 'SpinMenuOpen_Up', this);
        this.spinMenuOpen.anchor.set(0.5);
        this.spinMenuOpen.events.onInputUp.add(() => {
            this.switchAutoSpinMenu(true);
        }, this);
        this.spinMenuClose = this.game.add.button(1210, 590, 'UI', null, this, 'SpinMenuClose_Over', 'SpinMenuClose_Up', 'SpinMenuClose_Down', 'SpinMenuClose_Up', this);
        this.spinMenuClose.anchor.set(0.5);
        this.spinMenuClose.events.onInputUp.add(() => {
            this.switchAutoSpinMenu(false);
        }, this);
        this.spinStatusChangedSignal = new Phaser.Signal();
        this.keySpace = this.game.input.keyboard.addKey(Phaser.KeyCode.SPACEBAR);
        this.switchAutoSpinMenu(false);
        this.changeSpinStatus(SPIN_STATUS.IDLE);
    }
    changeSpinStatus(newStatus) {
        this.keySpace.onUp.removeAll();
        this.skipMask.events.onInputUp.removeAll();
        switch (newStatus) {
            case SPIN_STATUS.IDLE:
                this.spinStart.visible = this.betAdd.visible = this.betReduce.visible = this.spinMenuOpen.visible = true;
                this.spinStop.visible = this.takeWin.visible = false;
                this.keySpace.onUp.addOnce(() => {
                    this.changeSpinStatus(SPIN_STATUS.START);
                }, this);
                break;
            case SPIN_STATUS.START:
                this.spinStart.visible = this.betAdd.visible = this.betReduce.visible = this.autoSpin.visible = this.autoFastSpin.visible = this.spinMenuOpen.visible = this.spinMenuClose.visible = false;
                this.spinStop.visible = this.spinStop.inputEnabled = true;
                break;
            case SPIN_STATUS.STOP:
                break;
        }
        this.spinStatusChangedSignal.dispatch(newStatus);
    }
    switchAutoSpinMenu(status) {
        this.spinMenuOpen.visible = !status;
        this.spinMenuClose.visible = status;
        this.betAdd.visible = !status;
        this.betReduce.visible = !status;
        this.autoSpin.visible = status;
        this.autoFastSpin.visible = status;
    }
    changeDisplayCurrency() {
        this.displayCurrency = !this.displayCurrency;
        this.updateDataDisplayBoard();
    }
    updateDataDisplayBoard() {
        if (this.displayCurrency) {
            this.creditsText.setText("¥" + this.floatWithCommas(this.gameData.money / this.gameData.moneyFractionMultiple));
            this.winText.setText("¥" + this.floatWithCommas(this.win / this.gameData.moneyFractionMultiple));
            this.totalBetText.setText("¥" + this.numberWithCommas((this.gameData.line * this.gameData.betUnit * this.gameData.betMultiples[this.betMultiple]) / this.gameData.moneyFractionMultiple));
        }
        else {
            this.creditsText.setText(this.numberWithCommas(Math.floor(this.gameData.money / this.gameData.denom / this.gameData.moneyFractionMultiple)));
            this.winText.setText(this.numberWithCommas(Math.floor(this.win / this.gameData.denom / this.gameData.moneyFractionMultiple)));
            this.totalBetText.setText(this.numberWithCommas(this.gameData.line * this.gameData.betUnit * this.gameData.betMultiples[this.betMultiple]));
        }
        this.betMultipleText.setText("X" + this.gameData.betMultiples[this.betMultiple]);
    }
    numberWithCommas(value) {
        return Number(value).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
    floatWithCommas(value) {
        let parts = value.toString().split(".");
        parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        if (parts.length > 1) {
            parts[1] = parts[1].substring(0, 2);
            if (parts[1].length == 1) {
                parts[1] += "0";
            }
        }
        else {
            parts[1] = "00";
        }
        return parts.join(".");
    }
    initReels() {
        let reelSettings = this.game.cache.getJSON('ReelSettings');
        this.reels = new Array(reelSettings.Strip);
        for (let i = 0; i < reelSettings.Strip; i++) {
            this.reels[i] = new Reel(this.game);
            this.reels[i].init(i, reelSettings);
            this.reels[i].rollFinishedSignal.add(() => {
                this.reelRollFinished(i);
            }, this);
            this.add(this.reels[i]);
        }
    }
    sendSpinData(symbolResult) {
        this.reelFinishedCount = 0;
        for (let i = 0; i < symbolResult.length; i++) {
            this.reels[i].prepareToRolling(symbolResult[i]);
        }
    }
    reelRollFinished(stripIndex) {
        this.reelFinishedCount++;
        if (this.reelFinishedCount == this.reels.length) {
            console.log("finis");
        }
    }
}
var PACKET_CODE;
(function (PACKET_CODE) {
    PACKET_CODE[PACKET_CODE["GtoCGameError"] = 1001] = "GtoCGameError";
    PACKET_CODE[PACKET_CODE["CtoGJoinGame"] = 1002] = "CtoGJoinGame";
    PACKET_CODE[PACKET_CODE["GtoCJoinGame"] = 1005] = "GtoCJoinGame";
    PACKET_CODE[PACKET_CODE["GtoCSlotInit"] = 11001] = "GtoCSlotInit";
    PACKET_CODE[PACKET_CODE["CtoGSlotNGPlay"] = 11002] = "CtoGSlotNGPlay";
    PACKET_CODE[PACKET_CODE["GtoCSlotNGPlay"] = 11003] = "GtoCSlotNGPlay";
})(PACKET_CODE || (PACKET_CODE = {}));
var SPIN_STATUS;
(function (SPIN_STATUS) {
    SPIN_STATUS[SPIN_STATUS["IDLE"] = 0] = "IDLE";
    SPIN_STATUS[SPIN_STATUS["START"] = 1] = "START";
    SPIN_STATUS[SPIN_STATUS["STOP"] = 2] = "STOP";
    SPIN_STATUS[SPIN_STATUS["AUTO"] = 3] = "AUTO";
    SPIN_STATUS[SPIN_STATUS["AUTO_FAST"] = 4] = "AUTO_FAST";
    SPIN_STATUS[SPIN_STATUS["WAIT_FOR_CREDITING"] = 5] = "WAIT_FOR_CREDITING";
    SPIN_STATUS[SPIN_STATUS["WAIT_FOR_TAKE_WIN"] = 6] = "WAIT_FOR_TAKE_WIN";
    SPIN_STATUS[SPIN_STATUS["TAKE_WIN"] = 7] = "TAKE_WIN";
})(SPIN_STATUS || (SPIN_STATUS = {}));
var REEL_STATUS;
(function (REEL_STATUS) {
    REEL_STATUS[REEL_STATUS["IDLE"] = 0] = "IDLE";
    REEL_STATUS[REEL_STATUS["SPIN"] = 1] = "SPIN";
    REEL_STATUS[REEL_STATUS["ROLLING"] = 2] = "ROLLING";
    REEL_STATUS[REEL_STATUS["FINISHED"] = 3] = "FINISHED";
    REEL_STATUS[REEL_STATUS["SCORING"] = 4] = "SCORING";
    REEL_STATUS[REEL_STATUS["ENDING"] = 5] = "ENDING";
})(REEL_STATUS || (REEL_STATUS = {}));
class ErrorMessage extends Phaser.Group {
    constructor(game) {
        super(game);
        this.initialize();
    }
    initialize() {
        let errorMask = this.game.add.graphics(0, 0, this);
        errorMask.beginFill(0x000000, 1);
        errorMask.drawRect(0, 0, 1280, 768);
        errorMask.endFill();
        errorMask.inputEnabled = true;
        this.errorMessage = this.game.add.text(640, 384, "", { font: "bold 56px Arial", fill: "#ffffff", align: "center", wordWrap: true, wordWrapWidth: 1200 }, this);
        this.errorMessage.anchor.setTo(0.5);
        this.visible = false;
    }
    showError(errorMessage) {
        if (this.visible) {
            return;
        }
        this.game.input.keyboard.removeKey(Phaser.Keyboard.SPACEBAR);
        this.game.time.events.removeAll();
        this.errorMessage.setText(errorMessage != undefined ? errorMessage : "Connect Close");
        this.visible = true;
    }
}
class FreeGame extends BasicGameMod {
    initGame() {
        this.game.add.image(188, 134, 'FreeGameBackgroundBottom', null, this);
        this.game.add.image(0, 0, 'FreeGameBackground', null, this);
        this.initDataDisplayPanel();
    }
}
class GameEntry {
    constructor() {
        this.game = new Phaser.Game(1280, 768, Phaser.AUTO, "", {
            preload: this.preload,
            create: this.create
        });
    }
    preload() {
        this.game.load.json('GameConfig', 'gameConfig.json');
    }
    create() {
        this.game.stage.disableVisibilityChange = true;
        this.game.scale.pageAlignHorizontally = true;
        this.game.scale.pageAlignVertically = true;
        if (this.game.device.android) {
            this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
        }
        else {
            this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        }
        let gameConfig = this.game.cache.getJSON('GameConfig');
        let regexLanguage = /language=([^&]+)/;
        let matchLanguage = regexLanguage.exec(window.location.href);
        let allLanguages = gameConfig.LanguageSupport;
        let urlLanguage = matchLanguage != undefined ? (allLanguages.find((value, index, array) => {
            return matchLanguage[1] == value;
        }) ? matchLanguage[1] : allLanguages[0]) : allLanguages[0];
        let languageID = allLanguages.findIndex(languageID => languageID === urlLanguage);
        let languagePackName = gameConfig.LanguagePackName[languageID];
        document.title = gameConfig.GameTitle[languageID];
        this.game.state.add('Preloader', Preloader);
        this.game.state.start('Preloader', true, false, languagePackName);
    }
}
new GameEntry();
class GameLoader extends Phaser.State {
    constructor() {
        super();
    }
    init(languagePackName, platformName, server, gameData) {
        this.languagePackName = languagePackName;
        this.gameData = gameData;
        this.server = server;
        this.server.connectionSignal.add(this.connectionHandler, this);
        this.server.receiveSignal.add(this.receiveHandler, this);
        this.game.add.image(0, 0, 'LoadingBackground');
        if (platformName != "") {
            this.game.add.image(640, 700, platformName).anchor.set(0.5);
        }
        let loadingBarBG = this.game.add.graphics(0, 755);
        loadingBarBG.beginFill(0xc47300);
        loadingBarBG.drawRect(0, 0, 1280, 3);
        loadingBarBG.endFill();
        this.loadingBar = this.game.add.graphics(0, 755);
        this.loadingBar.beginFill(0xffff00);
        this.loadingBar.drawRect(0, 0, 1280, 3);
        this.loadingBar.endFill();
        this.loadingBar.scale.set(0, 1);
        this.errorMessage = new ErrorMessage(this.game);
        this.add.existing(this.errorMessage);
    }
    preload() {
        this.game.load.pack('ui', 'assets/generic/pack.json');
        this.game.load.pack('game', 'assets/generic/pack.json');
        this.game.load.pack('ui', 'assets/' + this.languagePackName + '/pack.json');
        this.game.load.pack('game', 'assets/' + this.languagePackName + '/pack.json');
        this.game.load.onFileComplete.add(this.fileComplete, this);
        this.game.load.onLoadComplete.add(this.loadCompleted, this);
    }
    fileComplete(progress, cacheKey, success, totalLoaded, totalFiles) {
        this.loadingBar.scale.set(progress / 100, 1);
    }
    loadCompleted() {
        this.game.load.onFileComplete.remove(this.fileComplete, this);
        this.game.load.onLoadComplete.remove(this.loadCompleted, this);
        this.server.connectionSignal.remove(this.connectionHandler);
        this.server.receiveSignal.remove(this.receiveHandler);
        this.game.state.add('GameManager', GameManager);
        this.game.state.start('GameManager', true, false, this.server, this.gameData);
    }
    connectionHandler(connectionStatus) {
        if (!connectionStatus) {
            this.errorMessage.showError();
        }
    }
    receiveHandler(data) {
        let json = JSON.parse(data);
        switch (json.Code) {
            case PACKET_CODE.GtoCGameError:
                this.errorMessage.showError(json.packet.errorMessage);
                this.server.close();
                break;
        }
    }
}
class GameManager extends Phaser.State {
    constructor() {
        super();
    }
    init(server, gameData) {
        this.gameData = gameData;
        this.server = server;
        this.server.connectionSignal.add(this.connectionHandler, this);
        this.server.receiveSignal.add(this.receiveHandler, this);
        this.normalGame = new NormalGame(this.game, this.gameData);
        this.normalGame.sendMessageSignal.add(this.sendHandler, this);
        this.freeGame = new FreeGame(this.game, this.gameData);
        this.freeGame.visible = false;
        this.errorMessage = new ErrorMessage(this.game);
        this.add.existing(this.errorMessage);
    }
    connectionHandler(connectionStatus) {
        if (!connectionStatus) {
            this.errorMessage.showError();
        }
    }
    receiveHandler(data) {
        let json = JSON.parse(data);
        switch (json.Code) {
            case PACKET_CODE.GtoCGameError:
                this.server.close();
                break;
            case PACKET_CODE.GtoCSlotNGPlay:
                console.log(json);
                this.normalGame.changeReelStatus(REEL_STATUS.ROLLING, json);
                break;
        }
    }
    sendHandler(data) {
        this.server.send(data);
    }
}
class NormalGame extends BasicGameMod {
    initGame() {
        this.game.add.image(188, 134, 'NormalGameBackgroundBottom', null, this);
        this.initReels();
        this.game.add.image(0, 0, 'NormalGameBackground', null, this);
        this.initDataDisplayPanel();
        this.initSpinControlPanel();
        this.spinStatusChangedSignal.add((newStatus) => {
            this.spinStatusChanged(newStatus);
        }, this);
        this.initMenuPanel();
        this.initInsufficientBalancePanel();
    }
    initMenuPanel() {
        this.menuOpen = this.game.add.button(60, 708, 'UI', null, this, 'Menu_Over', 'Menu_Up', 'Menu_Down', 'Menu_Up', this);
        this.menuOpen.anchor.set(0.5);
        this.menuOpen.onInputUp.add(() => {
            this.menuOpen.visible = false;
            menuGroup.visible = true;
        }, this);
        let menuGroup = this.game.add.group(this);
        menuGroup.visible = false;
        let mask = this.game.add.graphics(0, 0, menuGroup);
        mask.beginFill(0x000000, 0.6);
        mask.drawRect(0, 0, 1280, 768);
        mask.endFill();
        mask.inputEnabled = true;
        mask.events.onInputUp.add(() => {
            menuGroup.visible = false;
            this.menuOpen.visible = true;
        }, menuGroup);
        let gameConfig = this.game.cache.getJSON('GameConfig');
        this.game.add.sprite(0, 768, 'UI', 'Menu_BG_' + gameConfig.NumberOfMenuColumn, menuGroup).anchor.set(0, 1);
        let menuClose = this.game.add.button(60, 708, 'UI', null, this, 'MenuClose_Over', 'MenuClose_Up', 'MenuClose_Down', 'MenuClose_Up', menuGroup);
        menuClose.anchor.set(0.5);
        menuClose.onInputUp.add(() => {
            menuGroup.visible = false;
            this.menuOpen.visible = true;
        }, menuGroup);
        let instructionPageOpen = this.game.add.button(60, 594, 'UI', null, this, 'InstructionPage_Over', 'InstructionPage_Up', 'InstructionPage_Down', 'InstructionPage_Up', menuGroup);
        instructionPageOpen.anchor.set(0.5);
        instructionPageOpen.onInputUp.add(() => {
            instructionPageGroup.visible = true;
        }, menuGroup);
        let soundOpen = this.game.add.button(60, 480, 'UI', null, this, 'SoundOpen_Over', 'SoundOpen_Up', 'SoundOpen_Down', 'SoundOpen_Up', menuGroup);
        soundOpen.anchor.set(0.5);
        soundOpen.onInputUp.add(() => {
            soundOpen.visible = false;
            soundClose.visible = true;
        }, menuGroup);
        let soundClose = this.game.add.button(60, 480, 'UI', null, this, 'SoundClose_Over', 'SoundClose_Up', 'SoundClose_Down', 'SoundClose_Up', menuGroup);
        soundClose.anchor.set(0.5);
        soundClose.visible = false;
        soundClose.onInputUp.add(() => {
            soundClose.visible = false;
            soundOpen.visible = true;
        }, menuGroup);
        if (gameConfig.NumberOfMenuColumn == 5) {
            let historicalRecordOpen = this.game.add.button(60, 366, 'UI', null, this, 'HistoricalRecord_Over', 'HistoricalRecord_Up', 'HistoricalRecord_Down', 'HistoricalRecord_Up', menuGroup);
            historicalRecordOpen.anchor.set(0.5);
            let exit = this.game.add.button(60, 252, 'UI', null, this, 'Exit_Over', 'Exit_Up', 'Exit_Down', 'Exit_Up', menuGroup);
            exit.anchor.set(0.5);
        }
        let instructionPageGroup = this.game.add.group(this);
        instructionPageGroup.visible = false;
        this.initInstructionPage(gameConfig.NumberOfInstructionPages, instructionPageGroup);
    }
    initInstructionPage(numberOfPages, pageGroup) {
        let currentPage = 0;
        let previousPage = 0;
        let pages = new Array(numberOfPages);
        for (let i = 0; i < numberOfPages; i++) {
            pages[i] = this.game.add.image(0, 0, 'InstructionPage_' + (i + 1), null, pageGroup);
            pages[i].visible = i == currentPage ? true : false;
            pages[i].inputEnabled = true;
            pages[i].events.onInputUp.add(() => {
                currentPage = currentPage < numberOfPages - 1 ? currentPage + 1 : 0;
                changePage();
            }, pageGroup);
        }
        let pageClose = this.game.add.button(1220, 0, 'UI', null, this, 'InstructionPage_Close', 'InstructionPage_Close', 'InstructionPage_Close', 'InstructionPage_Close', pageGroup);
        pageClose.onInputDown.add(() => {
            pageClose.tint = 0x949494;
        }, pageGroup);
        pageClose.onInputUp.add(() => {
            pageClose.tint = 0xffffff;
            pageGroup.visible = false;
        }, pageGroup);
        let pagePrevious = this.game.add.button(0, 384, 'UI', null, this, 'InstructionPage_Previous', 'InstructionPage_Previous', 'InstructionPage_Previous', 'InstructionPage_Previous', pageGroup);
        pagePrevious.anchor.set(0, 0.5);
        pagePrevious.onInputDown.add(() => {
            pagePrevious.tint = 0x949494;
        }, pageGroup);
        pagePrevious.onInputUp.add(() => {
            pagePrevious.tint = 0xffffff;
            currentPage = currentPage > 0 ? currentPage - 1 : numberOfPages - 1;
            changePage();
        }, pageGroup);
        let pageNext = this.game.add.button(1280, 384, 'UI', null, this, 'InstructionPage_Next', 'InstructionPage_Next', 'InstructionPage_Next', 'InstructionPage_Next', pageGroup);
        pageNext.anchor.set(1, 0.5);
        pageNext.onInputDown.add(() => {
            pageNext.tint = 0x949494;
        }, pageGroup);
        pageNext.onInputUp.add(() => {
            pageNext.tint = 0xffffff;
            currentPage = currentPage < numberOfPages - 1 ? currentPage + 1 : 0;
            changePage();
        }, pageGroup);
        function changePage() {
            pages[currentPage].visible = true;
            pages[previousPage].visible = false;
            previousPage = currentPage;
        }
    }
    spinStatusChanged(newStatus) {
        switch (newStatus) {
            case SPIN_STATUS.START:
                this.changeReelStatus(REEL_STATUS.SPIN);
                break;
        }
    }
    changeReelStatus(newStatus, data) {
        switch (newStatus) {
            case REEL_STATUS.IDLE:
                break;
            case REEL_STATUS.SPIN:
                let currentTotalBet = this.gameData.line * this.gameData.betUnit * this.gameData.betMultiples[this.betMultiple];
                let currentCredits = this.gameData.money / this.gameData.denom / this.gameData.moneyFractionMultiple;
                if (currentCredits >= currentTotalBet) {
                    let packet = {
                        Code: PACKET_CODE.CtoGSlotNGPlay,
                        BetMultiple: this.betMultiple
                    };
                    this.sendMessageSignal.dispatch(JSON.stringify(packet));
                }
                else {
                    this.changeSpinStatus(SPIN_STATUS.IDLE);
                    this.insufficientBalanceGroup.visible = true;
                }
                break;
            case REEL_STATUS.ROLLING:
                this.roundIdText.setText(data.RoundID + "");
                break;
        }
    }
    initInsufficientBalancePanel() {
        this.insufficientBalanceGroup = this.game.add.group(this);
        this.insufficientBalanceGroup.visible = false;
        let mask = this.game.add.graphics(0, 0, this.insufficientBalanceGroup);
        mask.beginFill(0x000000, 0.6);
        mask.drawRect(0, 0, 1280, 768);
        mask.endFill();
        mask.inputEnabled = true;
        let languageSettings = this.game.cache.getJSON('LanguageSettings');
        this.game.add.sprite(640, 384, 'UI', 'InsufficientBalance_BG', this.insufficientBalanceGroup).anchor.set(0.5);
        this.game.add.text(640, 340, languageSettings.InsufficientBalanceTitle, { font: "bold 60px Microsoft JhengHei", fill: "#fff" }, this.insufficientBalanceGroup).anchor.set(0.5);
        let confirm = this.game.add.image(640, 450, 'InsufficientBalanceConfirm', null, this.insufficientBalanceGroup);
        confirm.anchor.set(0.5);
        confirm.inputEnabled = true;
        confirm.events.onInputDown.add(() => {
            confirm.tint = 0x949494;
        }, this.insufficientBalanceGroup);
        confirm.events.onInputUp.add(() => {
            confirm.tint = 0xffffff;
            this.insufficientBalanceGroup.visible = false;
        }, this.insufficientBalanceGroup);
    }
}
class Preloader extends Phaser.State {
    constructor() {
        super();
    }
    init(languagePackName) {
        this.languagePackName = languagePackName;
    }
    preload() {
        this.platformName = this.game.cache.getJSON('GameConfig').PlatformName;
        if (this.platformName != "") {
            this.game.load.image(this.platformName, 'assets/logo/' + this.platformName + '.png');
        }
        this.game.load.pack('loading', 'assets/' + this.languagePackName + '/pack.json');
        this.game.load.onLoadComplete.add(this.serverInitialize, this);
    }
    serverInitialize() {
        this.game.load.onLoadComplete.remove(this.serverInitialize, this);
        this.game.add.image(0, 0, 'LoadingBackground');
        if (this.platformName != "") {
            this.game.add.image(640, 700, this.platformName).anchor.set(0.5);
        }
        let serverIndex = this.game.cache.getJSON('GameConfig').ServerIndex;
        let serverURL = this.game.cache.getJSON('GameConfig').ServerURL;
        this.server = new Server(serverURL[serverIndex]);
        this.server.connectionSignal.add(this.connectionHandler, this);
        this.server.receiveSignal.add(this.receiveHandler, this);
        this.gameData = {};
        this.errorMessage = new ErrorMessage(this.game);
        this.add.existing(this.errorMessage);
    }
    connectionHandler(connectionStatus) {
        if (connectionStatus) {
            let regexToken = /token=(\w+)/;
            let matchToken = regexToken.exec(window.location.href);
            let gameID = this.game.cache.getJSON('GameConfig').GameID;
            let urlToken = matchToken != undefined ? matchToken[1] : "";
            let packet = {
                Code: PACKET_CODE.CtoGJoinGame,
                GameToken: urlToken,
                GameID: gameID
            };
            this.connectTimeoutsTimerEvent = this.game.time.events.add(10000, () => {
                this.server.close();
                this.errorMessage.showError();
            }, this);
            this.isLoginProcess = true;
            this.server.send(JSON.stringify(packet));
        }
        else {
            this.game.time.events.remove(this.connectTimeoutsTimerEvent);
            this.errorMessage.showError(this.isLoginProcess ? "Token Invalid" : undefined);
        }
    }
    receiveHandler(data) {
        let json = JSON.parse(data);
        switch (json.Code) {
            case PACKET_CODE.GtoCGameError:
                break;
            case PACKET_CODE.GtoCJoinGame:
                this.game.time.events.remove(this.connectTimeoutsTimerEvent);
                this.isLoginProcess = false;
                this.gameData.money = json.Money;
                break;
            case PACKET_CODE.GtoCSlotInit:
                this.gameData.moneyFractionMultiple = json.MoneyFractionMultiple;
                this.gameData.denom = json.Denom / 100;
                this.gameData.line = json.Line;
                this.gameData.betMultiples = json.BetMultiples;
                this.gameData.betUnit = json.BetUnit;
                this.server.connectionSignal.remove(this.connectionHandler);
                this.server.receiveSignal.remove(this.receiveHandler);
                this.game.state.add("GameLoader", GameLoader);
                this.game.state.start("GameLoader", true, false, this.languagePackName, this.platformName, this.server, this.gameData);
                break;
        }
    }
}
class Reel extends Phaser.Group {
    constructor(game) {
        super(game);
    }
    init(stripIndex, reelSettings) {
        this.rollFinishedSignal = new Phaser.Signal();
        this.settings = reelSettings;
        this.intervalSymbolGroup = new Array(2);
        this.intervalSymbolGroup[0] = this.game.add.group(this);
        this.intervalSymbolGroup[1] = this.game.add.group(this);
        this.mainSymbolGroup = new Array(2);
        this.mainSymbolGroup[0] = this.game.add.group(this);
        this.mainSymbolGroup[1] = this.game.add.group(this);
        this.previousGroupIndex = 0;
        this.nextGroupIndex = 1;
        this.offsetX = this.settings.PositionX[stripIndex];
        this.offsetY = this.settings.PositionY[stripIndex];
        this.intervalSymbolCount = this.settings.SymbolIntervalCount[stripIndex];
        this.mainSymbolCount = this.settings.InitialPermutations[stripIndex].length;
        this.symbolNames = ["WD", "SC", "FG", "S1", "S2", "S3", "S4", "A", "K", "Q", "J", "TE", "NI"];
        this.rollingTime = this.settings.RollingTime[stripIndex];
        this.mainSymbolGroup[this.previousGroupIndex].position.set(this.offsetX, this.offsetY);
        for (let i = 0; i < this.mainSymbolCount; i++) {
            let symbol = this.game.add.sprite(0, this.settings.SymbolHeight * i, 'Symbol', this.symbolNames[this.settings.InitialPermutations[stripIndex][i]], this.mainSymbolGroup[this.previousGroupIndex]);
            symbol.anchor.set(0.5);
        }
        let randomSymbol = this.generateRandomSymbol();
        this.intervalSymbolGroup[this.previousGroupIndex].position.set(this.offsetX, this.offsetY + this.settings.SymbolHeight * this.mainSymbolCount);
        for (let i = 0; i < this.intervalSymbolCount; i++) {
            let symbol = this.game.add.sprite(0, this.settings.SymbolHeight * i, 'Symbol', this.symbolNames[randomSymbol[i]], this.intervalSymbolGroup[this.previousGroupIndex]);
            symbol.anchor.set(0.5);
        }
    }
    prepareToRolling(mainSymbol) {
        this.intervalSymbolGroup[this.nextGroupIndex].children.length = 0;
        this.intervalSymbolGroup[this.nextGroupIndex].position.set(this.offsetX, this.offsetY - this.settings.SymbolHeight * this.intervalSymbolCount);
        let randomSymbol = this.generateRandomSymbol();
        for (let i = 0; i < this.intervalSymbolCount; i++) {
            let symbol = this.game.add.sprite(0, this.settings.SymbolHeight * i, 'Symbol', this.symbolNames[randomSymbol[i]], this.intervalSymbolGroup[this.nextGroupIndex]);
            symbol.anchor.set(0.5);
        }
        this.mainSymbolGroup[this.nextGroupIndex].children.length = 0;
        this.mainSymbolGroup[this.nextGroupIndex].position.set(this.offsetX, this.offsetY - this.settings.SymbolHeight * (this.intervalSymbolCount + this.mainSymbolCount));
        for (let i = 0; i < this.mainSymbolCount; i++) {
            let symbol = this.game.add.sprite(0, this.settings.SymbolHeight * i, 'Symbol', this.symbolNames[mainSymbol[i]], this.mainSymbolGroup[this.nextGroupIndex]);
            symbol.anchor.set(0.5);
        }
        if (this.settings.RollBeginningOffset > 0) {
            this.rollBeginningOffset();
        }
        else {
            this.startRolling();
        }
    }
    rollBeginningOffset() {
        let perFrameOffset = this.settings.RollBeginningOffset / (this.settings.RollBeginningTotalTime / 20);
        let intervalNextPosY = this.intervalSymbolGroup[this.previousGroupIndex].position.y;
        let intervalTargetPosY = intervalNextPosY - this.settings.RollBeginningOffset;
        let mainNextPosY = this.mainSymbolGroup[this.previousGroupIndex].position.y;
        let mainTargetPosY = mainNextPosY - this.settings.RollBeginningOffset;
        this.rollTimerEvent = this.game.time.events.loop(20, () => {
            intervalNextPosY -= perFrameOffset;
            mainNextPosY -= perFrameOffset;
            if (mainNextPosY <= mainTargetPosY) {
                this.game.time.events.remove(this.rollTimerEvent);
                this.intervalSymbolGroup[this.previousGroupIndex].position.set(this.offsetX, intervalTargetPosY);
                this.mainSymbolGroup[this.previousGroupIndex].position.set(this.offsetX, mainTargetPosY);
                this.startRolling();
                return;
            }
            this.intervalSymbolGroup[this.previousGroupIndex].position.set(this.offsetX, intervalNextPosY);
            this.mainSymbolGroup[this.previousGroupIndex].position.set(this.offsetX, mainNextPosY);
        }, this);
    }
    startRolling() {
        let perFrameOffset = (Math.abs(this.mainSymbolGroup[this.nextGroupIndex].position.y) + this.settings.RollBeginningOffset + this.settings.RollEndingOffset) / (this.rollingTime / 20);
        if (this.settings.RollBeginningOffset > 0) {
            let intervalCurrentPosY = this.intervalSymbolGroup[this.previousGroupIndex].position.y;
            let mainCurrentPosY = this.mainSymbolGroup[this.previousGroupIndex].position.y;
            let mainTargetPosY = mainCurrentPosY + this.settings.RollBeginningOffset;
            this.rollTimerEvent = this.game.time.events.loop(20, () => {
                intervalCurrentPosY += perFrameOffset;
                mainCurrentPosY += perFrameOffset;
                if (mainCurrentPosY >= mainTargetPosY) {
                    this.game.time.events.remove(this.rollTimerEvent);
                    this.mainSymbolGroup[this.previousGroupIndex].position.set(this.offsetX, mainTargetPosY);
                    this.generateNewIntervalSymbolGroup();
                    this.rolling(perFrameOffset);
                    return;
                }
                this.intervalSymbolGroup[this.previousGroupIndex].position.set(this.offsetX, intervalCurrentPosY);
                this.mainSymbolGroup[this.previousGroupIndex].position.set(this.offsetX, mainCurrentPosY);
            }, this);
        }
        else {
            this.generateNewIntervalSymbolGroup();
            this.rolling(perFrameOffset);
        }
    }
    rolling(perFrameOffset) {
        let intervalNextPosY = this.intervalSymbolGroup[this.nextGroupIndex].position.y;
        let intervalPreviousPosY = this.intervalSymbolGroup[this.previousGroupIndex].position.y;
        let intervalTargetPosY = this.offsetY - this.intervalSymbolCount * this.settings.SymbolHeight + this.settings.RollEndingOffset;
        let mainNextPosY = this.mainSymbolGroup[this.nextGroupIndex].position.y;
        let mainTargetPosY = this.offsetY + this.settings.RollEndingOffset;
        let mainPreviousPosY = this.mainSymbolGroup[this.previousGroupIndex].position.y;
        this.rollTimerEvent = this.game.time.events.loop(20, () => {
            intervalNextPosY += perFrameOffset;
            intervalPreviousPosY += perFrameOffset;
            mainNextPosY += perFrameOffset;
            mainPreviousPosY += perFrameOffset;
            if (mainNextPosY >= mainTargetPosY) {
                this.game.time.events.remove(this.rollTimerEvent);
                this.intervalSymbolGroup[this.previousGroupIndex].position.set(this.offsetX, intervalTargetPosY);
                this.mainSymbolGroup[this.nextGroupIndex].position.set(this.offsetX, mainTargetPosY);
                if (this.settings.RollEndingOffset > 0) {
                    this.rollEndingOffset();
                }
                else {
                    this.rollingEnd();
                }
                return;
            }
            this.intervalSymbolGroup[this.nextGroupIndex].position.set(this.offsetX, intervalNextPosY);
            this.intervalSymbolGroup[this.previousGroupIndex].position.set(this.offsetX, intervalPreviousPosY);
            this.mainSymbolGroup[this.nextGroupIndex].position.set(this.offsetX, mainNextPosY);
            this.mainSymbolGroup[this.previousGroupIndex].position.set(this.offsetX, mainPreviousPosY);
        }, this);
    }
    stopRolling() {
        let intervalTargetPosY = this.offsetY + this.mainSymbolCount * this.settings.SymbolHeight;
    }
    rollEndingOffset() {
        let perFrameOffset = this.settings.RollEndingOffset / (this.settings.RollEndingTotalTime / 20);
        let intervalPreviousPosY = this.intervalSymbolGroup[this.previousGroupIndex].position.y;
        let intervalTargetPosY = intervalPreviousPosY - this.settings.RollEndingOffset;
        let mainNextPosY = this.mainSymbolGroup[this.nextGroupIndex].position.y;
        let mainTargetPosY = mainNextPosY - this.settings.RollEndingOffset;
        this.rollTimerEvent = this.game.time.events.loop(20, () => {
            intervalPreviousPosY -= perFrameOffset;
            mainNextPosY -= perFrameOffset;
            if (mainNextPosY <= mainTargetPosY) {
                this.game.time.events.remove(this.rollTimerEvent);
                this.intervalSymbolGroup[this.previousGroupIndex].position.set(this.offsetX, intervalTargetPosY);
                this.mainSymbolGroup[this.nextGroupIndex].position.set(this.offsetX, mainTargetPosY);
                this.rollingEnd();
                return;
            }
            this.intervalSymbolGroup[this.previousGroupIndex].position.set(this.offsetX, intervalPreviousPosY);
            this.mainSymbolGroup[this.nextGroupIndex].position.set(this.offsetX, mainNextPosY);
        }, this);
    }
    rollingEnd() {
        this.previousGroupIndex = this.nextGroupIndex;
        this.nextGroupIndex = this.nextGroupIndex == 0 ? 1 : 0;
        this.rollFinishedSignal.dispatch();
    }
    generateNewIntervalSymbolGroup() {
        this.intervalSymbolGroup[this.previousGroupIndex].children.length = 0;
        this.intervalSymbolGroup[this.previousGroupIndex].position.set(this.offsetX, this.mainSymbolGroup[this.nextGroupIndex].position.y - this.intervalSymbolCount * this.settings.SymbolHeight);
        let randomSymbol = this.generateRandomSymbol();
        for (let i = 0; i < this.intervalSymbolCount; i++) {
            let symbol = this.game.add.sprite(0, this.settings.SymbolHeight * i, 'Symbol', this.symbolNames[randomSymbol[i]], this.intervalSymbolGroup[this.previousGroupIndex]);
            symbol.anchor.set(0.5);
        }
    }
    generateRandomSymbol() {
        let randomSymbol = new Array(this.intervalSymbolCount);
        for (let i = 0; i < this.intervalSymbolCount; i++) {
            randomSymbol[i] = Math.floor(Math.random() * 9) + 3;
        }
        return randomSymbol;
    }
}
class Server {
    constructor(url) {
        this.receiveSignal = new Phaser.Signal();
        this.connectionSignal = new Phaser.Signal();
        this.connect(url);
    }
    connect(url) {
        let self = this;
        this.websocket = new WebSocket(url);
        this.websocket.onopen = (event) => {
            self.connectionSignal.dispatch(true);
        };
        this.websocket.onmessage = (event) => {
            self.receiveSignal.dispatch(event.data);
        };
        this.websocket.onclose = (event) => {
            self.connectionSignal.dispatch(false);
        };
        this.websocket.onerror = (event) => {
            console.error(event);
        };
    }
    close() {
        this.websocket.close();
    }
    send(message) {
        this.websocket.send(message);
    }
}
